var fs = require( 'fs' );
var http = require( 'http' );
var parseUrl = require( 'url' ).parse;

var server = http.createServer( ( req, res ) => {
	const url = parseUrl( req.url );
	const pathname = url.pathname === '/' ? '/index.html' : url.pathname;
	const allowBlob = /allow-blob=true/.test( url.query );

	try {
		res.writeHead( 200, { 'Content-Security-Policy': `default-src 'self' 'unsafe-eval' https: data:` + ( allowBlob ? ' blob:' : '' ) });
		res.end( fs.readFileSync( `public${pathname}`, 'utf-8' ) );
	} catch ( err ) {
		res.writeHead( 404 );
		res.end( 'not found' );
	}
});

var PORT = 6789;

server.listen( PORT );
console.log( `listening on localhost:${PORT}` );
