mapboxgl.accessToken = 'pk.eyJ1IjoiZ2lhIiwiYSI6Imo0MGlwN0EifQ.lJnbqEqj60ejSGx9s4ht2w';
var map = new mapboxgl.Map({
	container: 'map', // container id
	style: 'mapbox://styles/mapbox/streets-v8', //stylesheet location
	center: [-74.50, 40], // starting position
	zoom: 9 // starting zoom
});
